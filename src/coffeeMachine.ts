import { Observable, BehaviorSubject, pipe, merge, fromEvent } from 'rxjs';
import { staticNever } from 'rxjs-compat/add/observable/never';
import { map, scan } from 'rxjs/operators';

interface CoffeeMachine {
    name: string;
    color?: string;
    hello(): void;
}

class coffee implements CoffeeMachine {
    waterMounts = 0;
    beans = 0;
    name: string = null;
    color = 'black';
    isWater: boolean = false;
    isBean: boolean = false;

    constructor(name: string) {
        this.name = name;
    }
    get water(): number {
        return this.waterMounts;
    }
    set water(value: number) {
        this.waterMounts = value;
        this.isWater = true;
    }
    hello(): void {
        alert(`안녕하세요 ${this.name} 커피머신 입니다`);
    }

    dropCoffee(): void {
        if ((this.isWater = false)) {
            alert('커피에 물이 없습니다.');
        } else if (this.waterMounts <= 0) {
            alert('커피에 물이 부족합니다');
            console.log(`${this.waterMounts} 물의 양`);
        } else {
            this.waterMounts -= 150;
            this.isWater = true;
            alert('커피 한잔 완성입니다.');
        }
    }

    dropEspresso(): void {
        if ((this.isWater = false)) {
            alert('커피에 물이 없습니다');
        } else if (this.waterMounts <= 0) {
            alert('커피에 물이 부족합니다');
            alert(`${this.waterMounts} 물의양`);
        } else {
            this.waterMounts -= 100;
            this.isWater = true;
            alert('에스프로세소 한잔 완성입니다');
        }
    }
    reset(): any {
        this.waterMounts = 0;
        this.isWater = false;
        console.log(this.waterMounts);
    }
}

const pil = new coffee('philips');

// window.onload = function (): void {   // 핫모듈 체험을 위해 잠시 베재
//     let observer = new Observable((subscriber) => {
//         try {
//             subscriber.next(pil.hello());
//             subscriber.next(console.log('made in movv'));
//             subscriber.complete();
//         } catch (e) {
//             subscriber.error(e);
//         }
//     });
//     observer.subscribe(() => console.log('발행완료'));
// };

//초기 실행시
const toggle = document.getElementById('water') as HTMLInputElement;
const beanState = document.getElementById('bean') as HTMLInputElement;

const observable: Observable<any> = new Observable((subcriber) => {
    subcriber.next((pil.water = 500));
    subcriber.next((toggle.checked = pil.isWater));
    subcriber.next(
        (document.querySelector('.waterMount').innerHTML =
            '물의 양 :' + pil.water + 'ml' + '<br/>'),
    );
    document.querySelector('.beanMount').innerHTML =
        '원두 양 : ' + pil.beans + 'g' + '<br/>';
    setTimeout(() => {
        log('커피 머신 준비가 완료 됐습니다.');
        subcriber.complete();
    }, 2000);
});
observable.subscribe({
    next(x) {
        console.log(console.log(x));
    },
    error(err) {
        console.log(err);
    },
    complete() {
        console.log('done');
    },
});

const chargeButton = document.querySelector('.charge');
const resetButton = document.querySelector('.exit');
const coffeeButton = document.querySelector('.coffee');
const esspresoButton = document.querySelector('.esspreso');
const beanButton = document.querySelector('.beanCharge');

let increase = fromEvent(chargeButton, 'click', (e) => {
    log('500ml 물을 충전하였습니다');
    pil.isWater = true;
}).pipe(
    map(
        () => (state) => Object.assign({}, state, { water: state.water + 500 }),
    ),
);

let beanCharge = fromEvent(beanButton, 'click', (e) => {
    log('원두 충전 완료 했습니다.');
    pil.isBean = true;
}).pipe(
    map(() => (state) => Object.assign({}, state, { bean: state.bean + 300 })),
);

let dropCoffee = fromEvent(coffeeButton, 'click', (e) => {
    log('커피 추출 중입니다.....');
    setTimeout(() => {
        pil.dropCoffee();
        log('커피 한잔 완성했습니다');
    }, 2000);
}).pipe(
    map(
        () => (state) =>
            Object.assign({}, state, {
                water: state.water - 100,
                bean: state.bean - 100,
            }),
    ),
);

let dropEspresso = fromEvent(esspresoButton, 'click', (e) => {
    log('에스프레소 추출 중입니다.');
    setTimeout(() => {
        pil.dropEspresso();
        log('에스프레소 한잔 완성했습니다');
    }, 2000);
}).pipe(
    map(
        () => (state) =>
            Object.assign({}, state, {
                water: state.water - 50,
                bean: state.bean - 50,
            }),
    ),
);

let reset = fromEvent(resetButton, 'click', (e) => {
    log('커피 머신이 종료 되었습니다.');
    pil.isWater = false;
}).pipe(
    map(
        () => (state) => Object.assign({}, state, { water: (state.water = 0) }),
    ),
);

let state = merge(increase, reset, dropCoffee, dropEspresso, beanCharge).pipe(
    scan((state, changFn) => changFn(state), {
        water: pil.waterMounts,
        bean: pil.beans,
    }),
);

state.subscribe((state) => {
    document.querySelector('.waterMount').innerHTML =
        '물의 양 : ' + state.water + 'ml' + '<br/>';
    document.querySelector('.beanMount').innerHTML =
        '원두 양 : ' + state.bean + 'g' + '<br/>';
    if (state.water <= 0) {
        toggle.checked = false;
    } else {
        toggle.checked = true;
    }
    if (state.bean <= 0) {
        beanState.checked = false;
    } else {
        beanState.checked = true;
    }
});

function log(msg: string): any {
    let logElem = document.querySelector('.log');

    let time: Date = new Date();
    let timeStr: string = time.toLocaleTimeString();
    logElem.innerHTML += timeStr + ': ' + msg + '<br/>';
}

log('커피 머신 로딩');

// const bean = document.querySelector('.bean');
// bean.innerHTML += pil.beans + '<br/>';
