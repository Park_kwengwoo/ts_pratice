let num: number = 10;
let isLoggedIn: boolean = false;
let arr: number[] = [1, 2, 3];
let arr1: Array<number> = [1, 2, 3];

//튜플 : 배열의 길이가 고정되고 각 요소의 타입이 지정되 있다.
let arr3: [string, number] = ['hi', 10];

let srt: any = 'hi';
srt = 5;
let arr5: any = ['a', 2, true];
let umm: void = undefined;
function mouse(): void {
    console.log('undefined');
}
mouse();
function sum(a: number, b: number): number {
    return a + b;
}
