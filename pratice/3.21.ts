interface Machine {
  name?: string;
  color?: string;
  partner?: string;
}

class coffeeMachine implements Machine {
  name: string = null;
  color: string = null;
  width: string;
  partner: string;

  constructor(name: string, color: string) {
    this.name = name;
    this.color = color;
  }

  getPartner(param: string): string {
    return (this.partner = param);
  }
  context(): void {
    console.log(
      `안녕하세요 이 커피 머신은 ${this.name}이며 브랜드는 ${this.partner} 입니다`,
    );
  }
}

function createMachine(config: Machine): { name: string } {
  let newMachine = { name: 'max' };
  return newMachine;
}

const v1 = new coffeeMachine('애슐라', '검정색');
v1.getPartner('SamSung');
v1.context();

//읽기전용 프로퍼티 : 일부 프로퍼티들은 생성될 때만 수정 가능해야합니다. 프로퍼티 이름 앞에 readonly를 넣어서 지정할 수 있습니다.

// interface Point {
//   readonly x: number;
//   readonly y: number;
// }

// let p5: Point = { x: 10, y: 20 };
// //p5.x=5 읽기전용 속성이므로 다시 선언 x

// let array: ReadonlyArray<number> = [1, 2, 3, 4];
// //array[0] = 2; 오류
