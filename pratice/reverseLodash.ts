let arr2: Array<any> = [1, 2, 3, [4, 5], 6, 7];

function _reverse(arr: Array<any>) {
  let newArr: Array<any> = [];
  let length: number = (<any>arr).length;
  for (let i: number = length; i > 0; i--) {
    if (Array.isArray(arr[i - 1])) {
      newArr.push(_reverse(arr[i - 1]));
    } else {
      newArr.push(arr[i - 1]);
    }
    console.log(newArr);
  }
  return newArr;
}

_reverse(arr2);
