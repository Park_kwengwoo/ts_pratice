//tuple 배열인데 타입이 한가지가 아닌 경우 , 마찬가지로 객체이다

const tu: [number, string] = [0, '1'];

enum Color {
  red,
  green,
  blue,
}
let c: Color = Color.blue;

//type assertions
//  '타입이 이것이다' 라고 컴파일러에게 알려주는 것을 의미
// 문법적으로는 두가지 방법이 있다.
// 1 .변주 as 강제할 타입
// <강제할 타입>변수

let someValue: any = 'this is a string';
let valueLength: number = (<string>someValue).length; // 강제 형변환
let valueLength1: number = (someValue as string).length;

//type allias
//인터페이스랑 비슷해 보인다.
//타입에 별명을 붙이는 거라 생각하면 된다.
//primitive, union type, tuple
// 기타 직접 작성해야하는 타입을 다른 이름을 지정할 수 있다.

console.log('union type 예시');
const a = () => {
  let a: any;
  let b: string | number; //union 할 type 많아지면 귀찮아 진다.
  type StringOrNumber = string | number;
  let c: StringOrNumber;
  b = 'st';
  b = 0;
};

console.log(a);

//interface

interface Person {
  name: string;
  age?: number; // ?을 붙이면 있어도 되고 없어도 된다느 뜻이다.
  hello?(): void;
}

const p: Person = {
  name: 'mark',
  hello: (): string => {
    return 'Hello';
  },
};

function hello(p: Person): void {
  console.log(`안녕하세요 ${p.name}`);
}

//optional property
//indexable type

interface Person2 {
  name: string;
  [index: string]: string; //index 에는 sting 혹은 number 밖에 사용 못한다.
  [index: number]: string; //array  같은 형식
}

const q: Person2 = {
  name: 'hi',
};

q.anybody = 'anna';
q[2] = 'amm'; //index 번호 제약을 준다

//class and interface

interface IPerson {
  name: string;
  hello(): void;
}

class Person3 implements IPerson {
  name: string = null;
  constructor(name: string) {
    this.name = name;
  }
  hello(): void {
    return console.log(`hi ${this.name}`);
  }
}

const p1: IPerson = new Person3('kwegnwoo');

p1.hello();

//상속 기능 추가
interface Korean extends IPerson {
  city: string;
}

class Person4 implements Korean {
  name: string = null;
  city: string = null;
  constructor(name: string, city: string) {
    this.name = name;
    this.city = city;
  }
  hello(): void {
    return console.log(`hi im ${this.name} and city ${this.city}`);
  }
}

const p4 = new Person4('kwengwoo', 'gwangju');
p4.hello();
